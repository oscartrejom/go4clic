@administrator @data
Feature: Formulario posterior a 'Login por 1ra vez' como Administrador
     
     Quiero llenar el formulario para acceder al sistema por primera vez como administrador

     Background:
          Given el Administrador hizo Login por 1ra vez exitoso
          And esta en la ventana de formulario

     @positive
     Scenario: Verificar llenado de Formulario exitoso 
          When ingresa un nombre en 'Nombre' que solo tenga letras
          And ingresa un apellido en 'Apellido' que solo tenga letras
          And ingresa un telefono en 'Telefono' que tenga entre 4 y 18 numeros
          And da clic en botón 'Guardar'
          Then el sistema mostrar el siguiente mensaje 'Felicitaciones Actualizaste correctamente tus datos'
          And accedera a la Pagina Principal como administrador
     
#formatos validos de Nombre=Apellido: "Nombre" , "Nombre Nombre" , "Nombre Nombre Nombre" , "aa" , "a a" , infinita cant de letras.
#formatos validos de Telefono: "+12 (345) 6789012345" , '+1 (3) 621' , '+12345678901234567' , '1(45454555544441' , '123456789012345678'


     @negative
     Scenario: Intentar llenar Formulario agregando espacio despues del nombre
          When ingresa en 'Nombre' el siguiente formato (ej: 'Nombre ') 
                    And ingresa un apellido en 'Apellido' que solo contenga letras
          And ingresa un telefono en 'Telefono'
          And da clic en botón 'Guardar'
          Then ???????????????????????????????????????

     @negative
     Scenario: Intentar llenar Formulario agregando espacios en Apellido
          When ingresa un noombre en 'Nombre' que solo contenga letras
          And ingresa un apellido en 'Apellido' que incluya letras y espacios
          And ingresa un telefono en 'Telefono'
          And da clic en botón 'Guardar'
          Then ???????????????????????????????????????


     #NECESITO LAS PRECONDICIONES DEL CAMPO TELEFONO PARA ESCENARIOS NEGATIVOS (ACEPTA ESPACIOS? SIGNOS? CANT DE NUMEROS?)
     @negative
     Scenario: Intentar llenar Formulario agregando  ESPACIOS? SIGNOS? X CANT DE NUMEROS? en Telefono
          When ingresa un nombre en 'Nombre' que solo contenga letras
          And ingresa un apellido en 'Apellido' que solo contenga letras
          And ingresa un telefono en 'Telefono'
          And da clic en botón 'Guardar'
          Then ???????????????????????????????????????

     @negative
     Scenario: Intentar llenar Formulario dejando Nombre vacio
          When ingresa un apellido en 'Apellido' que solo contenga letras
          And ingresa un telefono en 'Telefono'
          And da clic en botón 'Guardar'
          Then ???????????????????????????????????????

     @negative
     Scenario: Intentar llenar Formulario dejando Apellido vacio
          When ingresa un nombre en 'Nombre' que solo contenga letras
          And ingresa un telefono en 'Telefono'
          And da clic en botón 'Guardar'
          Then ???????????????????????????????????????

     @positive
     Scenario: Verificar llenado de Formulario exitoso dejando Telefono vacio
          When ingresa un nombre en 'Nombre' que solo contenga letras
          And ingresa un apellido en 'Apellido' que solo contenga letras
          And da clic en botón 'Guardar'
          Then el sistema mostrar el siguiente mensaje 'Felicitaciones Actualizaste correctamente tus datos'
          And accedera a la Pagina Principal como administrador

     @negative
     Scenario: Intentar llenar Formulario dejando todos los campos vacios
          When deja vacio todos los campos ('Nombre' , 'Apellido' y 'Telefono')
          And da clic en botón 'Guardar'
          Then ???????????????????????????????????????