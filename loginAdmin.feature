@administrator @data
Feature: Login administrador
    
     Como Administrador de go4clic quiero una web de login para acceder al sistema

     Background:
          Given el usuario esta en la web de Login

     @positive
     Scenario: Verificar que se hace Login exitoso
          When ingresa 'Email' y 'Contraseña' de un usuario que existe en la tabla "Nombre de la tabla Admins"
          And hacer clic en botón 'Ingresar'
          Then el sistema accedera a la Pagina Principal como administrador

     @negative
     Scenario: Intentar hacer login con formato de email incorrecto
          When ingresa 'Email' con formato incorrecto
          And ingresa 'Contraseña' al azar
          And da clic en botón 'Ingresar'
          Then el sistema no accedera a la Pagina Principal del usuario
          And monstrara el siguiente mensaje 'El formato del email no es correcto. Ej: hola@mail.com'

     @negative
     Scenario: Intentar hacer login con Usuario inexistente
          When ingresa 'Email' con formato valido pero inexistente en la tabla "Nombre de la tabla Admins"
          And ingresa 'Contraseña' al azar
          And da clic en botón 'Ingresar'
          Then el sistema no accedera a la Pagina Principal del usuario
          And monstrara el siguiente mensaje 'No se encontró una cuenta activa.'

     @negative
     Scenario: Intentar hacer login con Usuario vacio
          When solamente ingresa una 'Contraseña' al azar
          And da clic en botón 'Ingresar'
          Then el sistema no accedera a la Pagina Principal del usuario
          And mostrara el siguiente mensaje 'Es necesario completar el email'

     @negative
     Scenario: Intentar hacer login con Contraseña invalida
          When ingresa 'Email' de un usuario que existe en la tabla "Nombre de la tabla Admins"
          But ingresa una 'Contraseña' al azar
          And da clic en botón 'Ingresar'
          Then el sistema no accedera a la Pagina Principal del usuario
          And mostrara el siguiente mensaje 'Esta combinación de correo electrónico y contraseña es incorrecta.'

     @negative
     Scenario: Intentar hacer login con Email (con formato valido) y Contraseña vacia
          When solamente ingresa 'Email' al azar
          And da clic en botón 'Acceder'
          Then el sistema no accedera a la Pagina Principal del usuario
          And  el borde del input 'Contraseña' se pondra de color rojo
     #quizas deberia dar un mensaje como cuando el campo email esta vacio.

     @negative
     Scenario: Intentar hacer login con Email (con formato incorrecto) y Contraseña vacia
          When solamente ingresa 'Email' con formato incorrecto
          And da clic en botón 'Acceder'
          Then el sistema no accedera a la Pagina Principal del usuario
          And monstrara el siguiente mensaje 'El formato del email no es correcto. Ej: hola@mail.com'

     @negative
     Scenario: Intentar hacer login con todos los campos vacios
          When deja vacio todos los campos ('Email' y 'Contraseña')
          And da clic en botón 'Acceder'
          Then el sistema no accedera a la Pagina Principal del usuario
          And mostrara el siguiente mensaje 'Es necesario completar el email'
     #podria decir 'Es necesario completar el Email y Contraseña'

     @positive
     Scenario: Verificar que se hace Login por 1ra vez exitoso
          When ingresa 'Email' y 'Contraseña' de un usuario que existe en la tabla "Nombre de la tabla Admins" y nunca ha inciado sesion
          And hacer clic en botón 'Ingresar'
          Then el sistema accedera a la Pagina Principal como administrador
          But desplegara una ventana de 'Formulario'
