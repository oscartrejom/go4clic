@administrator @validation @positive
Feature: Validaciones de la Pagina principal como administrador
    
    Como Administrador quiero hacer validaciones de la pagina principal para asegurar que opera correctamente

    Background:
        Given el usuario como Administrador realizo login
        And se despliega la Pagina Principal

    # headerNav ----------------------------------------------------------------------------------------------------------

    @headerNav @graphical
    Scenario: Validar el headerNav
        Then estan las imagenes del botón Menu, la Academia, botón Tutorial de bienvenida, botón Notificaciones y botón de Usuario

    @headerNav  @button
    Scenario: Validar la ventana Menu
        When se hace clic en botón de Menu
        Then se desplega la ventana de Menu
        And en la ventana menu esta el nombre de la academia
        And estan los links de 'Configuracion', 'Admins', 'Cursos', 'Areas', 'Pasarelas de Pago' y 'Sobre Nosotros'
    #cuando abres un curso, luego vuelves a la Pagina Principal y abres esta ventana hay bug

    @headerNav @button
    Scenario: Validar la ventana de Usuario
        When se hace clic sobre el botón de Usuario
        Then se desplega la ventana del Usuario
        And en la ventana menu esta la imagen, nombre e email del usuario
        And estan los links de 'Mi Perfil', 'Gestionar Institucion', 'La(s) Academia(s),', 'Cerrar Sesion'

    @headerNav @button
    Scenario: Validar la ventana de Usuario
        When se hace clic sobre el botón de Usuario
        Then se desplega la ventana del Usuario
        And en la ventana menu esta la imagen, nombre y email del usuario
        And estan los links de 'Mi Perfil', 'Gestionar Institucion', 'La(s) Academia(s),', 'Cerrar Sesion'

    @headerNav @button
    Scenario: Validar la ventana de Notificaciones
        When se hace clic sobre el botón de notificaciones
        Then se desplega la ventana de notificaciones
        And en la ventana de Notificaciones esta el botón para volver y las notificaciones por leer

    @headerNav @button
    Scenario: Validar de las ventanas de Tutorial de bienvenida
        When se hace clic sobre el botón de Tutorial de bienvenida
        Then se desplega la ventana de Tutorial de bienvenida


    # headeHome -----------------------------------------------------------------------------------------------------

    @headerHome @graphical
    Scenario: Validar el headerHome
        Then de fondo esta la imagen y el background-color
        And esta el Título, Subtítuto, Descripción y el botón 'Conócenos'

    @headerHome @button
    Scenario: Validar el botón Conócenos
        When se hace clic sobre boton conocenos
        Then se despliega la Pagina "Sobre Nosotros"

    # courses --------------------------------------------------------------------------------------------------------

    @courses @graphical
    Scenario: Validar la seccion de cursos
        Then esta el parrafo 'PROYECTOS', el titulo 'Nuestra oferta' y el input buscar
        And esta los n cantidad de tarjetas de cursos publicados

    @courses @button
    Scenario: Hacer clic sobre una tarjeta de un curso publicado 
        When se hace clic sobre la tarjeta del curso publicado
        Then se despliega la Pagina del curso, seccion 'Unidades'

    # banner ---------------------------------------------------------------------------------------------------------

    @banner @graphical
    Scenario: Validar la seccion de banner
        Then de fondo esta la imagen, el background-color
        And esta la Frase y el botón 'Conócenos'

    @banner @button
    Scenario: Validar el botón Conócenos
        When se hace clic sobre boton conocenos
        Then se despliega la Pagina "Sobre Nosotros"

    # comments ---------------------------------------------------------------------------------------------------------

    #Redundante 'Testimonios' y 'Testimonios de participantes'
    @comments @graphical
    Scenario: Validar la seccion de testimonios
        Then esta el parrafo 'TESTIMONIOS', el titulo 'Testimonios de participantes'
        And esta los n cantidad de testimonios publicados

    # footer ---------------------------------------------------------------------------------------------------------

    @footer @graphical
    Scenario: Validar el footer 
        Then esta el parrafo '® 2021 -' y el enlace de 'Términos y Condiciones'
        And estan los iconos (Sitio Web, Correo Electronico, Instagram, Facebook, Twitter y Linkedin)

    
    @footer @hyperlink
    Scenario: Validar los hipervinculos del footer
        When hace clic en 'Terminos y Condiciones' y en cada uno de los iconos (Sitio Web, Correo Electronico, Instagram, Facebook, Twitter y Linkedin)
        Then se abre una nueva pestaña por cada clic, correspondiente a 'go4clic Terminos y Condiciones' y cada una de las redes sociales de la Academia.

# ---------------------------------------------------------------------------------------------------------

    @button
    Scenario: Validar el boton de Whatsapp comercial
        When se hace clic en la imagen de whatsapp 
        Then se abre en una nueva pestaña el enlace de Chat de Whatsapp

   